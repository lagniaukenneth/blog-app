import datetime
from unittest import TestCase

from blog import Blog
from post import Post


class TestBlog(TestCase):
    def test_create_post(self):
        dummy_blog = Blog(
            "test",
            "test",
            []
        )

        post_title = "test"
        post_content = "test"
        # post_timestamp = "timestamp"

        now = datetime.datetime.now()
        dummy_blog.create_post(post_title, post_content)
        actual_post = dummy_blog.posts[0]

        self.assertEqual(1, len(dummy_blog.posts))
        self.assertEqual("test", dummy_blog.posts[0].content)
        self.assertEqual("test", dummy_blog.posts[0].title)
        self.assertIs(type(actual_post), Post)

        self.assertEqual(now, dummy_blog.posts[0].creation_timestamp)


