from unittest import TestCase
from post import *
import datetime


class TestPost(TestCase):
    def test_create_post(self):
        current_time = datetime.datetime.now()
        my_post = Post("test", "test", current_time)

        self.assertEqual("test", my_post.title)
        self.assertEqual("test", my_post.content)
        self.assertEqual(current_time, my_post.creation_timestamp)









