from unittest import TestCase
from blog import *


class TestBlog(TestCase):
    def test_create_blog(self):
        dummy_blog = Blog(
            "test",
            "test",
            []
        )
        self.assertEqual("test", dummy_blog.title)
        self.assertEqual("test", dummy_blog.author)
        self.assertListEqual([], dummy_blog.posts)

    def test_string_representation(self):
        dummy_blog = Blog(
            "test title",
            "test content",
            []
        )

        expected_result = "test title by test content - 0"
        actual_result = dummy_blog.__str__()

        self.assertEqual(expected_result, dummy_blog.__str__())
        self.assertEqual(expected_result, actual_result)
