import datetime

from post import Post


class Blog:
    def __init__(self, title, author, posts):
        self.title = title
        self.author = author
        self.posts = posts

    def create_post(self, title, content):
        now = datetime.datetime.now()
        created_post = Post(title, content, now)
        self.posts.append(created_post)

    def __str__(self):
        return f"{self.title} by {self.author} - {len(self.posts)}"


    