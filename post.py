

class Post:
    def __init__(self, title, content, creation_timestamp):
        self.title = title
        self.content = content
        self.creation_timestamp = creation_timestamp

    def str(self):
        return f"{self.title} by {self.content} at {self.creation_timestamp}"

