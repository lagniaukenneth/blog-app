from blog import *

blogs = [
    Blog("De wensen", "Kenneth Lagniau", [])
]


def run_menu():
    display_blogs()
    prompt = "Enter 'c' to create a blog, 'l' to list all the blogs, 'r' to read a blog," \
             " 'p' to create a post, or 'q' to quit"
    print(prompt)
    user_select = input("Enter your choice")
    while user_select != "q":
        if user_select == "c":
            ask_create_blog()
        elif user_select == "i":
            display_blogs()
        elif user_select == "r":
            ask_read_blog()
        elif user_select == "p":
            ask_create_post()
        else:
            print("Invalid input!")


def display_blogs():
    for blog in blogs:
        print(blog)


def ask_create_blog():
    created_blog_title = input("Enter a title for your new blog: ")
    created_blog_name = input("Enter your name")

    created_blog = Blog(
        created_blog_title,
        created_blog_name,
        []
    )
    blogs.append(created_blog)


def ask_read_blog():
    search_blog = input("Choose the blog you want to read (enter the title): ")
    for blog in blogs:
        correct_blog_name = (search_blog == blog.title)
        if correct_blog_name:
            print_posts_in_blog(blog)
            break


def print_posts_in_blog(blog):
    print(blog)
    for post in blog.posts:
        print(f"{post.title}")
        print(f"{post.content}")


def ask_create_post():
    input_user = input("Enter your name")
    for blog in blogs:
        correct_blog_author = (input_user == blog.author)
        if correct_blog_author:
            print(f"Hello {correct_blog_author}")
            print(f"You are currently creating a new post for {blog.title}")
            new_post_title = input("Enter the title of your new post: ")
            new_content = input("Enter the content of your new post: ")
            new_post = blog.create_post(new_post_title, new_content)
            print(new_post)


run_menu()
